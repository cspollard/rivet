Name: BABAR_2015_I1337783
Year: 2015
Summary: CP asymmetry in $\bar{B}\to X_{s+D}\gamma$ decays
Experiment: BABAR
Collider: PEP-II
InspireID: 1337783
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 114 (2015) 15, 151601
RunInfo: $B^-$ and $\bar{B}^0$ mesons in Upsilon(4S) decays (needed as photon energy measured in Upsilon(4S) rest frame)
Description:
  'Measurement of the CP asymmetry in $\bar{B}\to X_{s+D}\gamma$ decays'
ValidationInfo:
  'Herwig7 events using EvtGen for decays'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Belle:2015vct
BibTeX: '@article{Belle:2015vct,
    author = "Pesantez, L. and others",
    collaboration = "Belle",
    title = "{Measurement of the direct $CP$ asymmetry in $\bar{B}\rightarrow X_{s+d}\gamma$ decays with a lepton tag}",
    eprint = "1501.01702",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "KEK-PREPRINT-2014-37, KEK Preprint 2014-37",
    doi = "10.1103/PhysRevLett.114.151601",
    journal = "Phys. Rev. Lett.",
    volume = "114",
    number = "15",
    pages = "151601",
    year = "2015"
}
'
