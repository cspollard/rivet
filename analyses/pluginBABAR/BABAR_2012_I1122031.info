Name: BABAR_2012_I1122031
Year: 2012
Summary: Measurement of the hadronic mass spectrum for $\bar{B}\to X_s \gamma$
Experiment: BABAR
Collider: PEP-II
InspireID: 1122031
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 86 (2012) 052012
RunInfo: any process making $B^-$ and $\bar{B}^0$ mesons, eg. particle gun or $\Upslion(4S)$ decay. 
Description:
  'Hadropnic mass spectrum in $B\to s\gamma$ decays measured by BELLE. Useful for testing the implementation of these decays'
ValidationInfo:
  'Herwig 7 event using EvtGen for decays'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BaBar:2012eja
BibTeX: '@article{BaBar:2012eja,
    author = "Lees, J. P. and others",
    collaboration = "BaBar",
    title = "{Exclusive Measurements of $b \to s\gamma$ Transition Rate and Photon Energy Spectrum}",
    eprint = "1207.2520",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "BABAR-PUB-11-027, SLAC-PUB-15135",
    doi = "10.1103/PhysRevD.86.052012",
    journal = "Phys. Rev. D",
    volume = "86",
    pages = "052012",
    year = "2012"
}
'
