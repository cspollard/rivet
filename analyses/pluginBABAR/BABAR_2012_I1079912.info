Name: BABAR_2012_I1079912
Year: 2012
Summary: Lepton momentum spectrum in $B\to X_u\ell\nu$ decays
Experiment: BABAR
Collider: PEP-II
InspireID: 1079912
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 86 (2012) 032004
RunInfo: Any process producing B+, B0 mesons, originally Upsilon(4S) decay
Description:
  'Measurement of the branching ratio for $B\bar{X}\to X_u \ell\bar\nu_\ell$ for different lower cuts on the lepton momentum.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BaBar:2011xxm
BibTeX: '@article{BaBar:2011xxm,
    author = "Lees, J. P. and others",
    collaboration = "BaBar",
    title = "{Study of $\bar{B}\to X_u \ell \bar{\nu}$ decays in $B\bar{B}$ events tagged by a fully reconstructed B-meson decay and determination of $\|V_{ub}\|$}",
    eprint = "1112.0702",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "BABAR-PUB-11-014, SLAC-PUB-14811",
    doi = "10.1103/PhysRevD.86.032004",
    journal = "Phys. Rev. D",
    volume = "86",
    pages = "032004",
    year = "2012"
}
'
