Name: LHCB_2016_I1394391
Year: 2016
Summary:  Dalitz plot analysis of $D^0\to K^0_SK^\pm\pi^\mp$
Experiment: LHCB
Collider: LHC
InspireID: 1394391
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 -     Phys.Rev.D 93 (2016) 5, 052018
Description:
  'Measurement of Kinematic distributions  in the decays $D^0\to K^0_SK^\pm\pi^\mp$. The data were extracted from the plots in the paper.
   Resolution/acceptance effects have been not unfolded but an efficiency function base on Fig 4 of the paper is applied. Given the agreement with the model in the paper this analysis should only be used for qualitative studies.'
ValidationInfo:
  'Compared Herwig events using the same model for the decay as in the paper.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2015lnk
BibTeX: '@article{LHCb:2015lnk,
    author = "Aaij, Roel and others",
    collaboration = "LHCb",
    title = "{Studies of the resonance structure in $D^0\to K^0_S K^{\pm}\pi^{\mp}$ decays}",
    eprint = "1509.06628",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CERN-PH-EP-2015-238, LHCB-PAPER-2015-026",
    doi = "10.1103/PhysRevD.93.052018",
    journal = "Phys. Rev. D",
    volume = "93",
    number = "5",
    pages = "052018",
    year = "2016"
}
'
