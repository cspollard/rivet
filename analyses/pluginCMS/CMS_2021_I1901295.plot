# BEGIN PLOT /CMS_2021_I1901295/d159-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d161-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d163-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{l})$ [GeV]
YLabel=$\frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{l})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d165-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{l})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{l})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d167-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{high})$ [GeV]
YLabel=$\frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{high})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d169-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{high})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{high})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d171-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{low})$ [GeV]
YLabel=$\frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{low})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d173-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{low})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{low})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d175-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ S_\mathrm{T}$ [GeV]
YLabel=$\frac{d\sigma}{d S_\mathrm{T}}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d177-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ S_\mathrm{T}$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d S_\mathrm{T}}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d179-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$|y(\mathrm{t}_\mathrm{h})|$
YLabel=$\frac{d\sigma}{d|y(\mathrm{t}_\mathrm{h})|}$ [$\mathrm{pb}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d181-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$|y(\mathrm{t}_\mathrm{h})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d|y(\mathrm{t}_\mathrm{h})|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d183-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$|y(\mathrm{t}_\mathrm{l})|$
YLabel=$\frac{d\sigma}{d|y(\mathrm{t}_\mathrm{l})|}$ [$\mathrm{pb}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d185-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$|y(\mathrm{t}_\mathrm{l})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d|y(\mathrm{t}_\mathrm{l})|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d187-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}|$
YLabel=$\frac{d\sigma}{d\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}|}$ [$\mathrm{pb}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d189-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d191-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|$
YLabel=$\frac{d\sigma}{d|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|}$ [$\mathrm{pb}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d193-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d195-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{d\sigma}{dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d197-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d199-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d201-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d203-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$|y(\mathrm{t}\mathrm{\bar{t}})|$
YLabel=$\frac{d\sigma}{d|y(\mathrm{t}\mathrm{\bar{t}})|}$ [$\mathrm{pb}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d205-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$|y(\mathrm{t}\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d|y(\mathrm{t}\mathrm{\bar{t}})|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d207-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$\phi_{\mathrm{t}/\mathrm{\bar{t}}}$ [$\circ$]
YLabel=$\frac{d\sigma}{d\phi_{\mathrm{t}/\mathrm{\bar{t}}}}$ [$\mathrm{pb}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d209-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$\phi_{\mathrm{t}/\mathrm{\bar{t}}}$ [$\circ$]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d\phi_{\mathrm{t}/\mathrm{\bar{t}}}}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d211-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$\cos(\theta^*)$
YLabel=$\frac{d\sigma}{d\cos(\theta^*)}$ [$\mathrm{pb}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d213-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$\cos(\theta^*)$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d\cos(\theta^*)}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d215-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0\,$<$\,$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$\,$<$\,80\,GeV
XLabel=$|y(\mathrm{t}_\mathrm{h})|$
YLabel=$\frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h}) d|y(\mathrm{t}_\mathrm{h})|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d216-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 80\,$<$\,$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$\,$<$\,160\,GeV
XLabel=$|y(\mathrm{t}_\mathrm{h})|$
YLabel=$\frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h}) d|y(\mathrm{t}_\mathrm{h})|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d217-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 160\,$<$\,$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$\,$<$\,240\,GeV
XLabel=$|y(\mathrm{t}_\mathrm{h})|$
YLabel=$\frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h}) d|y(\mathrm{t}_\mathrm{h})|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d218-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 240\,$<$\,$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$\,$<$\,320\,GeV
XLabel=$|y(\mathrm{t}_\mathrm{h})|$
YLabel=$\frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h}) d|y(\mathrm{t}_\mathrm{h})|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d219-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 320\,$<$\,$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$\,$<$\,400\,GeV
XLabel=$|y(\mathrm{t}_\mathrm{h})|$
YLabel=$\frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h}) d|y(\mathrm{t}_\mathrm{h})|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d220-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 400\,$<$\,$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$\,$<$\,1500\,GeV
XLabel=$|y(\mathrm{t}_\mathrm{h})|$
YLabel=$\frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h}) d|y(\mathrm{t}_\mathrm{h})|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d222-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0\,$<$\,$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$\,$<$\,80\,GeV
XLabel=$|y(\mathrm{t}_\mathrm{h})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h}) d|y(\mathrm{t}_\mathrm{h})|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d223-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 80\,$<$\,$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$\,$<$\,160\,GeV
XLabel=$|y(\mathrm{t}_\mathrm{h})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h}) d|y(\mathrm{t}_\mathrm{h})|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d224-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 160\,$<$\,$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$\,$<$\,240\,GeV
XLabel=$|y(\mathrm{t}_\mathrm{h})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h}) d|y(\mathrm{t}_\mathrm{h})|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d225-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 240\,$<$\,$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$\,$<$\,320\,GeV
XLabel=$|y(\mathrm{t}_\mathrm{h})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h}) d|y(\mathrm{t}_\mathrm{h})|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d226-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 320\,$<$\,$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$\,$<$\,400\,GeV
XLabel=$|y(\mathrm{t}_\mathrm{h})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h}) d|y(\mathrm{t}_\mathrm{h})|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d227-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 400\,$<$\,$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$\,$<$\,1500\,GeV
XLabel=$|y(\mathrm{t}_\mathrm{h})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h}) d|y(\mathrm{t}_\mathrm{h})|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d229-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 250\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,420\,GeV
XLabel=$|y(\mathrm{t}\mathrm{\bar{t}})|$
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|y(\mathrm{t}\mathrm{\bar{t}})|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d230-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 420\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,520\,GeV
XLabel=$|y(\mathrm{t}\mathrm{\bar{t}})|$
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|y(\mathrm{t}\mathrm{\bar{t}})|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d231-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 520\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,620\,GeV
XLabel=$|y(\mathrm{t}\mathrm{\bar{t}})|$
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|y(\mathrm{t}\mathrm{\bar{t}})|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d232-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 620\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,800\,GeV
XLabel=$|y(\mathrm{t}\mathrm{\bar{t}})|$
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|y(\mathrm{t}\mathrm{\bar{t}})|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d233-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 800\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,1000\,GeV
XLabel=$|y(\mathrm{t}\mathrm{\bar{t}})|$
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|y(\mathrm{t}\mathrm{\bar{t}})|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d234-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1000\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,3500\,GeV
XLabel=$|y(\mathrm{t}\mathrm{\bar{t}})|$
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|y(\mathrm{t}\mathrm{\bar{t}})|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d236-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 250\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,420\,GeV
XLabel=$|y(\mathrm{t}\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|y(\mathrm{t}\mathrm{\bar{t}})|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d237-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 420\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,520\,GeV
XLabel=$|y(\mathrm{t}\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|y(\mathrm{t}\mathrm{\bar{t}})|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d238-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 520\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,620\,GeV
XLabel=$|y(\mathrm{t}\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|y(\mathrm{t}\mathrm{\bar{t}})|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d239-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 620\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,800\,GeV
XLabel=$|y(\mathrm{t}\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|y(\mathrm{t}\mathrm{\bar{t}})|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d240-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 800\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,1000\,GeV
XLabel=$|y(\mathrm{t}\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|y(\mathrm{t}\mathrm{\bar{t}})|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d241-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1000\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,3500\,GeV
XLabel=$|y(\mathrm{t}\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|y(\mathrm{t}\mathrm{\bar{t}})|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d243-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 250\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,420\,GeV
XLabel=$\cos(\theta^*)$
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d\cos(\theta^*)}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d244-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 420\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,520\,GeV
XLabel=$\cos(\theta^*)$
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d\cos(\theta^*)}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d245-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 520\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,620\,GeV
XLabel=$\cos(\theta^*)$
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d\cos(\theta^*)}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d246-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 620\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,800\,GeV
XLabel=$\cos(\theta^*)$
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d\cos(\theta^*)}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d247-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 800\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,1000\,GeV
XLabel=$\cos(\theta^*)$
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d\cos(\theta^*)}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d248-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1000\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,3500\,GeV
XLabel=$\cos(\theta^*)$
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d\cos(\theta^*)}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d250-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 250\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,420\,GeV
XLabel=$\cos(\theta^*)$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d\cos(\theta^*)}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d251-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 420\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,520\,GeV
XLabel=$\cos(\theta^*)$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d\cos(\theta^*)}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d252-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 520\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,620\,GeV
XLabel=$\cos(\theta^*)$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d\cos(\theta^*)}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d253-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 620\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,800\,GeV
XLabel=$\cos(\theta^*)$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d\cos(\theta^*)}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d254-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 800\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,1000\,GeV
XLabel=$\cos(\theta^*)$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d\cos(\theta^*)}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d255-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1000\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,3500\,GeV
XLabel=$\cos(\theta^*)$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d\cos(\theta^*)}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d257-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 250\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,420\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d258-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 420\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,520\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d259-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 520\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,620\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d260-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 620\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,800\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d261-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 800\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,1000\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d262-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1000\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,3500\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d264-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 250\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,420\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d265-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 420\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,520\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d266-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 520\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,620\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d267-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 620\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,800\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d268-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 800\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,1000\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d269-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1000\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,3500\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d271-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0\,$<$\,$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,50\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d272-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 50\,$<$\,$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,120\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d273-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 120\,$<$\,$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,200\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d274-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 200\,$<$\,$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,300\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d275-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 300\,$<$\,$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,400\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d276-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 400\,$<$\,$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,1200\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d278-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0\,$<$\,$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,50\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d279-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 50\,$<$\,$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,120\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d280-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 120\,$<$\,$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,200\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d281-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 200\,$<$\,$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,300\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d282-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 300\,$<$\,$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,400\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d283-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 400\,$<$\,$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,1200\,GeV
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}}) d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-2}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d285-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0\,$<$\,$|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|$\,$<$\,0.6\,
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d286-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0.6\,$<$\,$|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|$\,$<$\,1.2\,
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d287-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1.2\,$<$\,$|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|$\,$<$\,1.8\,
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d288-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1.8\,$<$\,$|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|$\,$<$\,3.5\,
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d290-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0\,$<$\,$|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|$\,$<$\,0.6\,
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d291-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0.6\,$<$\,$|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|$\,$<$\,1.2\,
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d292-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1.2\,$<$\,$|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|$\,$<$\,1.8\,
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d293-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1.8\,$<$\,$|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|$\,$<$\,3.5\,
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{dm(\mathrm{t}\mathrm{\bar{t}}) d|\Delta y_{\mathrm{t}/\mathrm{\bar{t}}}|}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d295-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 250\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,700\,GeV
XLabel=$\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}|$
YLabel=$\frac{d^2\sigma}{d\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}| dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d296-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 700\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,1000\,GeV
XLabel=$\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}|$
YLabel=$\frac{d^2\sigma}{d\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}| dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d297-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1000\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,3500\,GeV
XLabel=$\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}|$
YLabel=$\frac{d^2\sigma}{d\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}| dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d299-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 250\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,700\,GeV
XLabel=$\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}| dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d300-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 700\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,1000\,GeV
XLabel=$\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}| dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d301-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1000\,$<$\,$m(\mathrm{t}\mathrm{\bar{t}})$\,$<$\,3500\,GeV
XLabel=$\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d\Delta| y_{\mathrm{t}/\mathrm{\bar{t}}}| dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d303-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0\,$<$\,$|y(\mathrm{t})|$\,$<$\,0.5\,
XLabel=$|y(\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d|y(\mathrm{\bar{t}})| d|y(\mathrm{t})|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d304-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0.5\,$<$\,$|y(\mathrm{t})|$\,$<$\,1\,
XLabel=$|y(\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d|y(\mathrm{\bar{t}})| d|y(\mathrm{t})|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d305-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1\,$<$\,$|y(\mathrm{t})|$\,$<$\,1.5\,
XLabel=$|y(\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d|y(\mathrm{\bar{t}})| d|y(\mathrm{t})|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d306-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1.5\,$<$\,$|y(\mathrm{t})|$\,$<$\,2\,
XLabel=$|y(\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d|y(\mathrm{\bar{t}})| d|y(\mathrm{t})|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d307-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 2\,$<$\,$|y(\mathrm{t})|$\,$<$\,2.5\,
XLabel=$|y(\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d|y(\mathrm{\bar{t}})| d|y(\mathrm{t})|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d311-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0\,$<$\,$|y(\mathrm{t})|$\,$<$\,0.5\,
XLabel=$|y(\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d|y(\mathrm{\bar{t}})| d|y(\mathrm{t})|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d312-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0.5\,$<$\,$|y(\mathrm{t})|$\,$<$\,1\,
XLabel=$|y(\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d|y(\mathrm{\bar{t}})| d|y(\mathrm{t})|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d313-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1\,$<$\,$|y(\mathrm{t})|$\,$<$\,1.5\,
XLabel=$|y(\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d|y(\mathrm{\bar{t}})| d|y(\mathrm{t})|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d314-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1.5\,$<$\,$|y(\mathrm{t})|$\,$<$\,2\,
XLabel=$|y(\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d|y(\mathrm{\bar{t}})| d|y(\mathrm{t})|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d315-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 2\,$<$\,$|y(\mathrm{t})|$\,$<$\,2.5\,
XLabel=$|y(\mathrm{\bar{t}})|$
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d^2\sigma}{d|y(\mathrm{\bar{t}})| d|y(\mathrm{t})|}$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d317-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ p_\mathrm{T}(\ell)$ [GeV]
YLabel=$\frac{d\sigma}{d p_\mathrm{T}(\ell)}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d319-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ p_\mathrm{T}(\ell)$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d p_\mathrm{T}(\ell)}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d321-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=Additional jets
YLabel=$\sigma$ [$\mathrm{pb}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d323-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=Additional jets
YLabel=$\sigma$
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d325-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ H_\mathrm{T}$ [GeV]
YLabel=$\frac{d\sigma}{d H_\mathrm{T}}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d327-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ H_\mathrm{T}$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d H_\mathrm{T}}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d329-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ m_\mathrm{evt}$ [GeV]
YLabel=$\frac{d\sigma}{d m_\mathrm{evt}}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d331-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$
XLabel=$ m_\mathrm{evt}$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d m_\mathrm{evt}}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d333-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0 Additional jets
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{d\sigma}{dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d334-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1 Additional jets
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{d\sigma}{dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d335-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 2 Additional jets
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{d\sigma}{dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d336-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 3 Additional jets
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{d\sigma}{dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d338-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0 Additional jets
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d339-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1 Additional jets
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d340-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 2 Additional jets
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d341-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 3 Additional jets
XLabel=$m(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{dm(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d343-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d344-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d345-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 2 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d346-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 3 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d348-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d349-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d350-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 2 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d351-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 3 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}\mathrm{\bar{t}})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d353-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d354-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d355-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 2 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d356-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 3 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{pb}\,\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d358-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 0 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d359-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 1 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d360-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 2 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT

# BEGIN PLOT /CMS_2021_I1901295/d361-x01-y01
Title=CMS, 13\,TeV, $\mathrm{t}\mathrm{\bar{t}}$ $e/\mu +\mathrm{jets}$: 3 Additional jets
XLabel=$ p_\mathrm{T}(\mathrm{t}_\mathrm{h})$ [GeV]
YLabel=$\frac{1}{\sigma_\mathrm{norm}} \frac{d\sigma}{d p_\mathrm{T}(\mathrm{t}_\mathrm{h})}$ [$\mathrm{GeV}^{-1}$]
RatioPlotYLabel=$\frac{\mathrm{theory}}{\mathrm{data}}$
RatioPlotYMin=0.5
RatioPlotYMax=1.5
#END PLOT


