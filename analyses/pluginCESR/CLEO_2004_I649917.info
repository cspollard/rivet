Name: CLEO_2004_I649917
Year: 2004
Summary: Dalitz decay of $D^0\to K^0_S\pi^0\eta$
Experiment: CLEO
Collider: CESR
InspireID: 649917
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 93 (2004) 111801
RunInfo: Any process producing D0 mesons
Description:
  'Measurement of the mass distributions in the decay $D^0\to K^0_S\pi^0\eta$ by CLEO. The data were read from the plots in the paper. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events using model in the paper'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CLEO:2004umu
BibTeX: '@article{CLEO:2004umu,
    author = "Rubin, P. and others",
    collaboration = "CLEO",
    title = "{First observation and Dalitz analysis of the D0 ---\ensuremath{>} K0(S) eta pi0 decay}",
    eprint = "hep-ex/0405011",
    archivePrefix = "arXiv",
    reportNumber = "CLNS-04-1871, CLEO-04-5",
    doi = "10.1103/PhysRevLett.93.111801",
    journal = "Phys. Rev. Lett.",
    volume = "93",
    pages = "111801",
    year = "2004"
}
'
