Name: CLEO_2017_I1519168
Year: 2017
Summary: Mass distributions in the decays $D^0\to \pi^+\pi^-\pi^+\pi^-$ and $D^0\to K^+K^-\pi^+\pi^-$
Experiment: CLEO
Collider: CESR
InspireID: 1519168
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 05 (2017) 143
RunInfo: Any process producing D0 mesons
Description:
  'Measurement of the mass distributions in the decays $D^0\to \pi^+\pi^-\pi^+\pi^-$ and $D^0\to K^+K^-\pi^+\pi^-$ using data from CLEO. The data were read from the plots in the paper and therefore for some points the error bars are the size of the point. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: dArgent:2017gzv
BibTeX: '@article{dArgent:2017gzv,
    author = "dArgent, Philippe and Skidmore, Nicola and Benton, Jack and Dalseno, Jeremy and Gersabeck, Evelina and Harnew, Sam and Naik, Paras and Prouve, Claire and Rademacker, Jonas",
    title = "{Amplitude Analyses of $D^0 \to {\pi^+\pi^-\pi^+\pi^-}$ and $D^0 \to {K^+K^-\pi^+\pi^-}$ Decays}",
    eprint = "1703.08505",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1007/JHEP05(2017)143",
    journal = "JHEP",
    volume = "05",
    pages = "143",
    year = "2017"
}
'
